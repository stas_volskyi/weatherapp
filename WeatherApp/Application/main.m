//
//  main.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
