//
//  AppHelper.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "AppHelper.h"
#import "AppDelegate.h"
#import "City.h"

static NSString *const UseCelsiumMetrics = @"UseCelsiumMetrics";

@implementation AppHelper

+ (BookmarkStorage *)bookmarkStorage
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    return appDelegate.bookmarkStorage;
}

+ (NSArray<City *> *)knownCities
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    return appDelegate.knownCities;
}

#pragma mark - Error Handling

+ (void)displayErrorMessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:closeAction];
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

+ (void)displayError:(NSError *)error
{
    [self displayErrorMessage:error.localizedDescription];
}

#pragma mark - Resource

+ (NSArray<City *> *)loadCitiesList
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Cities" ofType:@"json"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    NSError *parseError;
    NSArray *json = [NSJSONSerialization JSONObjectWithData:fileData options:kNilOptions error:&parseError];
    
    NSParameterAssert(parseError == nil);
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dict in json) {
        City *city = [[City alloc] initWithDictionary:dict];
        if (city) {
            [array addObject:city];
        }
    }
    
    return array;
}

#pragma mark - Parse

+ (NSInteger)getInteger:(id)obj
{
    if ([obj respondsToSelector:@selector(integerValue)]) {
        return [obj integerValue];
    }
    return 0;
}

+ (long long)getLong:(id)obj
{
    if ([obj respondsToSelector:@selector(longLongValue)]) {
        return [obj longLongValue];
    }
    return 0;
}

+ (CGFloat)getFloat:(id)obj
{
    if ([obj respondsToSelector:@selector(floatValue)]) {
        return [obj floatValue];
    }
    return 0;
}

+ (CGFloat)getDouble:(id)obj
{
    if ([obj respondsToSelector:@selector(doubleValue)]) {
        return [obj doubleValue];
    }
    return 0;
}

+ (NSString *)getString:(id)obj
{
    if ([obj isKindOfClass:[NSString class]] && ![((NSString *)obj).lowercaseString isEqualToString:@"null"]) {
        return (NSString *)obj;
    }
    if ([obj respondsToSelector:@selector(stringValue)]) {
        return [obj stringValue];
    }
    return @"";
}

#pragma mark - Metrics

+ (void)useCelsiumMetrics:(BOOL)useCelsium
{
    [[NSUserDefaults standardUserDefaults] setBool:useCelsium forKey:UseCelsiumMetrics];
}

+ (CGFloat)formatTempWithMetrics:(CGFloat)kDegree
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:UseCelsiumMetrics]) {
        return kDegree - 273.15;
    }
    return kDegree * 9.0/5.0 - 459.67;
}

+ (BOOL)isCelsiumMetricsUsed
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:UseCelsiumMetrics];
}

#pragma mark - UI

+ (void)animateButton:(UIView *)view
{
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    anim.values = @[@1, @0.5, @1.2, @1];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.removedOnCompletion = YES;
    [view.layer addAnimation:anim forKey:nil];
}

@end
