//
//  AppHelper.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class City;
@class BookmarkStorage;

@interface AppHelper : NSObject

+ (BookmarkStorage *)bookmarkStorage;
+ (NSArray<City *> *)knownCities;

#pragma mark - Error Handling

+ (void)displayErrorMessage:(NSString *)message;
+ (void)displayError:(NSError *)error;

#pragma mark - Resource

+ (NSArray<City *> *)loadCitiesList;

#pragma mark - Parse

+ (NSInteger)getInteger:(id)obj;
+ (long long)getLong:(id)obj;
+ (CGFloat)getFloat:(id)obj;
+ (CGFloat)getDouble:(id)obj;
+ (NSString *)getString:(id)obj;

#pragma mark - Metrics

+ (void)useCelsiumMetrics:(BOOL)useCelsium;
+ (CGFloat)formatTempWithMetrics:(CGFloat)kDegree;
+ (BOOL)isCelsiumMetricsUsed;

#pragma mark - UI

+ (void)animateButton:(UIView *)view;

@end
