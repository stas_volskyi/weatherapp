//
//  AppDelegate.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class BookmarkStorage;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BookmarkStorage *bookmarkStorage;
@property (strong, nonatomic) NSArray<City *> *knownCities;

@end

