//
//  AppDelegate.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "AppDelegate.h"
#import "BookmarkStorage.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    self.bookmarkStorage = [BookmarkStorage new];
    self.knownCities = [AppHelper loadCitiesList];
    [[AppHelper bookmarkStorage] createDefaultBookmarks];
    return YES;
}

@end
