//
//  CityTableViewController.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class Forecast;
@class Weather;

@interface CityTableViewController : UITableViewController

@property (strong, nonatomic) Weather *weather;

@end
