//
//  CityTableViewController.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "CityTableViewController.h"
#import "DayForecastCollectionViewCell.h"

#import <MapKit/MapKit.h>
#import "WeatherClient.h"
#import "BookmarkStorage.h"
#import "Forecast.h"
#import "City.h"
#import "Weather.h"

static NSString *const DayForecastCellID = @"DayForecastCollectionViewCell";

@interface CityTableViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) WeatherClient *weatherClient;
@property (strong, nonatomic) BookmarkStorage *bookmarkStorage;
@property (strong, nonatomic) UIButton *bookmarkButton;

@property (strong, nonatomic) NSArray<Forecast *> *dailyForecast;

@property (weak, nonatomic) NSURLSessionTask *imageDownloadtask;
@property (assign, nonatomic) CGSize newSize;
@property (assign, nonatomic) CGFloat inset;

@end

@implementation CityTableViewController

#pragma mark - Livecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.weatherClient = [WeatherClient new];
    self.bookmarkStorage = [AppHelper bookmarkStorage];
    self.newSize = [UIScreen mainScreen].bounds.size;
    [self setupNavigationBar];
    
    __weak typeof (self) weakSelf = self;
    [self.weatherClient load5DayWeatherForCityWithId:self.weather.relatedCity.uniqueId completion:^(NSArray<Forecast *> *forecastArray, NSError *error) {
        weakSelf.dailyForecast = forecastArray;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.collectionView.collectionViewLayout invalidateLayout];
            });
        });
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self showInfo];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.imageDownloadtask cancel];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    self.newSize = size;
    [self.collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark - Private

- (void)setupNavigationBar
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    UIButton *bookmarkButton = [[UIButton alloc] init];
    [bookmarkButton setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
    [bookmarkButton setImage:[UIImage imageNamed:@"bookmarkSelected"] forState:UIControlStateSelected];
    bookmarkButton.tintColor = [UIColor whiteColor];
    [bookmarkButton addTarget:self action:@selector(bookmarkButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [bookmarkButton sizeToFit];
    self.bookmarkButton = bookmarkButton;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bookmarkButton];
}

- (void)showInfo
{
    self.cityNameLabel.text = self.weather.relatedCity.name;
    self.weatherDescriptionLabel.text = self.weather.currentForecast.weatherDescription;
    self.tempLabel.text = [NSString stringWithFormat:@"%.1f°", [AppHelper formatTempWithMetrics:self.weather.currentForecast.temp]];
    self.humidityLabel.text = [NSString stringWithFormat:@"%zd %%", self.weather.currentForecast.humidity];
    self.pressureLabel.text = [NSString stringWithFormat:@"%.0f hpa", self.weather.currentForecast.pressure];
    self.windLabel.text = [NSString stringWithFormat:@"%.1f m/s", self.weather.currentForecast.windSpeed];

    __weak typeof(self) weakSelf = self;
    self.imageDownloadtask = [self.weatherClient downloadWeatherImageWithId:self.weather.currentForecast.iconName completion:^(UIImage *image, NSError *error) {
        if (error) {
            NSLog(@"Download Image error %@", error);
        } else {
            weakSelf.imageView.image = image;
        }
    }];
    self.bookmarkButton.selected = [self.bookmarkStorage isCityInBookmarks:self.weather.relatedCity];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(self.weather.relatedCity.location.coordinate, 7000, 7000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:YES];
}

#pragma mark - Actions

- (void)bookmarkButtonAction:(UIButton *)button
{
    [AppHelper animateButton:button];
    if (button.selected) {
        [self.bookmarkStorage removeBookmark:self.weather.relatedCity];
    } else {
        [self.bookmarkStorage addBookmark:self.weather.relatedCity];
    }
    button.selected = !button.selected;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 140;
    if (indexPath.row == 1) {
        height = 60;
    } else if (indexPath.row == 2) {
        height = 70;
    } else if (indexPath.row == 3)  {
        height = MAX(self.tableView.bounds.size.height - self.tableView.contentInset.top - 270, 270);
    }
    return height;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dailyForecast.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DayForecastCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DayForecastCellID forIndexPath:indexPath];
    [cell displayForecastInfo:self.dailyForecast[indexPath.row]];
    __weak DayForecastCollectionViewCell *weakCell = cell;
    [self.weatherClient downloadWeatherImageWithId:self.dailyForecast[indexPath.row].iconName completion:^(UIImage *image, NSError *error) {
        weakCell.iconImageView.image = image;
    }];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (self.collectionView.contentSize.width <= 0) {
        return UIEdgeInsetsZero;
    }
    CGFloat sideInsets = self.newSize.width - (self.collectionView.contentSize.width - self.inset * 2);
    if (sideInsets > 0) {
        self.inset = sideInsets / 2.0;
        return UIEdgeInsetsMake(0, sideInsets / 2.0, 0, sideInsets / 2.0);
    } else {
        self.inset = 0;
        return UIEdgeInsetsZero;
    }
}

@end
