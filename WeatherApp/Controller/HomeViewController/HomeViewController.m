//
//  HomeViewController.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "HomeViewController.h"
#import "CityTableViewController.h"

// Views
#import "AddCityTableViewCell.h"
#import "CityTableViewCell.h"

#import "BookmarkStorage.h"
#import "Weather.h"
#import "City.h"
#import "Forecast.h"
#import "WeatherClient.h"

static NSString *const AddCityCellIdentifier = @"AddCityTableViewCell";
static NSString *const CityCellIdentifier = @"CityTableViewCell";

@interface HomeViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray<Weather *> *dataSource;
@property (strong, nonatomic) WeatherClient *weatherClient;

@property (strong, nonatomic) NSURLSessionTask *weatherTask;

@end

@implementation HomeViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"";
    self.weatherClient = [WeatherClient new];
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareDataSource];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.tableView setEditing:NO animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0) {
        cell = [self addCityCellForIndexPath:indexPath];
    } else {
        cell = [self cityCellForIndexPath:indexPath];
    }
    return cell;
}

#pragma mark - Cells

- (AddCityTableViewCell *)addCityCellForIndexPath:(NSIndexPath *)indexPath
{
    AddCityTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:AddCityCellIdentifier forIndexPath:indexPath];
    return cell;
}

- (CityTableViewCell *)cityCellForIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CityCellIdentifier forIndexPath:indexPath];
    Weather *weather = self.dataSource[indexPath.row - 1];
    [cell displayWeatherInfo:weather];
    if (weather.currentForecast) {
        __weak CityTableViewCell *weakCell = cell;
        cell.downloadImageTask = [self.weatherClient downloadWeatherImageWithId:weather.currentForecast.iconName completion:^(UIImage *image, NSError *error) {
            weakCell.weatherImageView.image = image;
        }];
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    Weather *weather;
    if (tableView == self.tableView) {
        if (indexPath.row == 0) {
            [self showSearchBar];
        } else {
            weather = self.dataSource[indexPath.row - 1];
            CityTableViewController *cityVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CityTableViewController"];
            cityVC.weather = weather;
            if (weather.currentForecast) {
                [self.navigationController pushViewController:cityVC animated:YES];
            } else {
                [self.weatherTask cancel];
                self.weatherTask = [self.weatherClient loadWeatherForCityWithId:weather.relatedCity.uniqueId completion:^(Forecast *forecast, NSError *error) {
                    if (error) {
                        [AppHelper displayError:error];
                    } else if (forecast) {
                        weather.currentForecast = forecast;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController pushViewController:cityVC animated:YES];
                        });
                    }
                }];
            }
        }
    } else {
        City *city = self.resultController.dataSource[indexPath.row];
        if (![[AppHelper bookmarkStorage] addBookmark:city]) {
            [self hideSearchBar];
            return;
        }
        
        
        __block Weather *weather = [Weather new];
        weather.relatedCity = city;
        [self.weatherClient loadWeatherForCityWithId:city.uniqueId completion:^(Forecast *forecast, NSError *error) {
            weather.currentForecast = forecast;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadRow:[self.dataSource indexOfObject:weather] + 1];
            });
        }];
        [self hideSearchBar];
        
        [self.tableView beginUpdates];
        [self.dataSource addObject:weather];
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.dataSource.count inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView) {
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[CityTableViewCell class]]) {
        [((CityTableViewCell *)cell).downloadImageTask cancel];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Weather *weather = self.dataSource[indexPath.row - 1];
    [[AppHelper bookmarkStorage] removeBookmark:weather.relatedCity];
    [self.tableView beginUpdates];
    [self.dataSource removeObject:weather];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

#pragma mark - Private

- (void)prepareUI
{
    self.tableView.tableFooterView = [UIView new];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)prepareDataSource
{
    NSArray<City *> *bookmarks = [AppHelper bookmarkStorage].getBookmarks;
    NSMutableArray<Weather *> *dataSource = [NSMutableArray new];
    
    for (City *city in bookmarks) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"relatedCity.uniqueId == %li", city.uniqueId];
        NSArray *currentDataSource = self.dataSource.copy;
        Weather *weatherInMemory = [currentDataSource filteredArrayUsingPredicate:predicate].firstObject;
        if (weatherInMemory.currentForecast) {
            [dataSource addObject:weatherInMemory];
            continue;
        }
        
        __block Weather *weather = [Weather new];
        weather.relatedCity = city;
        [dataSource addObject:weather];
        [self.weatherClient loadWeatherForCityWithId:city.uniqueId completion:^(Forecast *forecast, NSError *error) {
            weather.currentForecast = forecast;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadRow:[self.dataSource indexOfObject:weather] + 1];
            });
        }];
    }
    self.dataSource = dataSource;
    [self.tableView reloadData];
}

- (void)reloadRow:(NSInteger)row
{
    if (row == NSNotFound) {
        return;
    }
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

- (void)hideSearchBar
{
    self.searchController.active = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            self.searchController.searchBar.alpha = 0;
        }];
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
        self.tableView.tableHeaderView = nil;
    });
}

- (void)showSearchBar
{
    self.searchController.searchBar.alpha = 1;
    [self.tableView setContentOffset:CGPointMake(0, -44) animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.tableView.tableHeaderView = self.searchController.searchBar;
        self.searchController.active = YES;
    });
}

#pragma mark - UISearchControllerDelegate

- (void)willPresentSearchController:(UISearchController *)searchController
{
    self.tableView.userInteractionEnabled = NO;
}

- (void)willDismissSearchController:(UISearchController *)searchController
{
    self.tableView.userInteractionEnabled = YES;
}

- (void)didPresentSearchController:(UISearchController *)searchController
{
    [self.searchController.searchBar becomeFirstResponder];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self hideSearchBar];
}

@end
