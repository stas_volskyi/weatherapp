//
//  SettingsTableViewController.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "BookmarkStorage.h"

@interface SettingsTableViewController ()

@property (weak, nonatomic) IBOutlet UIButton *celsiumButton;
@property (weak, nonatomic) IBOutlet UIButton *farengeitButton;

@end

@implementation SettingsTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"";
    BOOL isCelsiumused = [AppHelper isCelsiumMetricsUsed];
    self.celsiumButton.selected = isCelsiumused;
    self.farengeitButton.selected = !isCelsiumused;
}

#pragma mark - Actions

- (IBAction)celsiumButtonAction:(UIButton *)sender
{
    [AppHelper animateButton:sender];
    if (sender.selected) {
        return;
    }
    sender.selected = !sender.selected;
    self.farengeitButton.selected = !self.farengeitButton.selected;
    [AppHelper useCelsiumMetrics:sender.selected];
}

- (IBAction)farengeitButtonAction:(UIButton *)sender
{
    [AppHelper animateButton:sender];
    if (sender.selected) {
        return;
    }
    sender.selected = !sender.selected;
    self.celsiumButton.selected = !self.celsiumButton.selected;
    [AppHelper useCelsiumMetrics:!sender.selected];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1 && indexPath.row == 0) {
        [self clearBookmarks];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
}

#pragma mark - Private

- (void)clearBookmarks
{
    [self askToClearBookmarksWithCompletion:^(BOOL delete) {
        if (delete) {
            [AppHelper.bookmarkStorage clearBookmarks];
        }
    }];
}

- (void)askToClearBookmarksWithCompletion:(void (^)(BOOL delete))completion
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Clear bookmarks" message:@"All your bookmarks will be deleted. You couldn't undo this action" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completion(NO);
    }];
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completion(YES);
    }];
    [alert addAction:cancelAction];
    [alert addAction:clearAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
