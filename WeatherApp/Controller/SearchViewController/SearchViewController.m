//
//  SearchViewController.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "SearchViewController.h"
#import "CityTableViewController.h"

#import <MapKit/MapKit.h>

#import "WeatherClient.h"
#import "City.h"
#import "Weather.h"

@class Forecast;

@interface SearchViewController () <UITableViewDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *searchBarContainer;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIButton *addPinButton;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) WeatherClient *weatherClient;

@property (strong, nonatomic) MKPointAnnotation *currentPin;
@property (strong, nonatomic) City *selectedCity;

@end

@implementation SearchViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.weatherClient = [WeatherClient new];
    self.tapGestureRecognizer.enabled = NO;
    self.navigationItem.title = @"";
    [self.tabBarController setSelectedIndex:1];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.searchController.searchBar.frame = self.searchBarContainer.bounds;
}

#pragma mark - Setup

- (void)setupSearchBar
{
    [super setupSearchBar];
    
    [self.searchBarContainer addSubview:self.searchController.searchBar];
    self.searchController.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    City *selectedCity = self.resultController.dataSource[indexPath.row];
    self.selectedCity = selectedCity;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(selectedCity.location.coordinate, 7000, 7000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    if (self.currentPin) {
        [self.mapView removeAnnotation:self.currentPin];
    }
    self.currentPin = [self addNewPinForCoordinate:selectedCity.location.coordinate];
    self.currentPin.title = selectedCity.name;
    
    [self.mapView addAnnotation:self.currentPin];
    self.searchController.active = NO;
    
    [self.mapView selectAnnotation:self.currentPin animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

#pragma mark - Private

- (MKPointAnnotation *)addNewPinForCoordinate:(CLLocationCoordinate2D)coordinate
{
    MKPointAnnotation *annotation = [MKPointAnnotation new];
    annotation.coordinate = coordinate;
    return annotation;
}

- (void)showPinButton:(BOOL)show
{
    [UIView animateWithDuration:0.2 animations:^{
        self.addPinButton.alpha = show;
    }];
}

- (void)displayLoader:(BOOL)dislplay
{
    if (dislplay) {
        self.mapView.userInteractionEnabled = NO;
        [self.activityIndicator startAnimating];
    } else {
        self.mapView.userInteractionEnabled = YES;
        [self.activityIndicator stopAnimating];
    }
    
    [UIView animateWithDuration:0.1 animations:^{
        self.loadingView.alpha = dislplay;
    }];
}

- (void)loadDetailsForSelectedCity
{
    [self displayLoader:YES];
    [self.weatherClient loadWeatherForCityWithId:self.selectedCity.uniqueId completion:^(Forecast *forecast, NSError *error) {
        if (error) {
            [self performSelectorOnMainThread:@selector(showError:) withObject:error waitUntilDone:NO];
        } else {
            [self performSelectorOnMainThread:@selector(displayDetailsWithForecast:) withObject:forecast waitUntilDone:NO];
        }
    }];
}

- (void)loadDetailsForAnnotation:(MKPointAnnotation *)annotation
{
    [self displayLoader:YES];
    [self.weatherClient loadWeatherForLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude completion:^(Forecast *forecast, NSError *error) {
        if (error) {
            [self performSelectorOnMainThread:@selector(showError:) withObject:error waitUntilDone:NO];
        } else {
            [self performSelectorOnMainThread:@selector(displayDetailsWithForecast:) withObject:forecast waitUntilDone:NO];
        }
    }];
}

- (void)displayDetailsWithForecast:(Forecast *)forecast
{
    [self displayLoader:NO];
    Weather *weather = [Weather new];
    if (self.selectedCity) {
        weather.relatedCity = self.selectedCity;
    } else {
        weather.relatedCity = [[City alloc] initWithForecast:forecast];
    }
    weather.currentForecast = forecast;
    
    CityTableViewController *cityVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CityTableViewController"];
    cityVC.weather = weather;
    [self.navigationController pushViewController:cityVC animated:YES];
}

- (void)showError:(NSError *)error
{
    [self displayLoader:NO];
    
    [AppHelper displayError:error];
}
     
#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];
    BOOL dragble = (annotation.title.length == 0);
    view.draggable = dragble;
    if (dragble) {
        [view setDragState:MKAnnotationViewDragStateDragging animated:YES];
    } else {
        view.canShowCallout = YES;
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        infoButton.tintColor = self.view.backgroundColor;
        view.rightCalloutAccessoryView = infoButton;
        
    }
    return view;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if (self.selectedCity) {
        [self loadDetailsForSelectedCity];
    } else {
        [self loadDetailsForAnnotation:view.annotation];
    }
}

#pragma mark - Actions

- (IBAction)addPinButtonAction:(id)sender
{
    if (self.addPinButton.selected) {
        CLLocationCoordinate2D coordinate = self.currentPin.coordinate;
        [self.mapView removeAnnotation:self.currentPin];
        CLGeocoder *geocoder = [CLGeocoder new];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            self.currentPin = [self addNewPinForCoordinate:coordinate];
            self.currentPin.title = placemarks.firstObject.locality;
            if (placemarks.firstObject.locality) {
                self.currentPin.title = placemarks.firstObject.locality;
            } else if (placemarks.firstObject.administrativeArea) {
                self.currentPin.title = placemarks.firstObject.administrativeArea;
            } else {
                self.currentPin.title = placemarks.firstObject.name;
            }

            [self.mapView addAnnotation:self.currentPin];
        }];
        self.addPinButton.selected = NO;
    } else {
        if (self.currentPin) {
            [self.mapView removeAnnotation:self.currentPin];
            self.selectedCity = nil;
        }
        self.tapGestureRecognizer.enabled = YES;
        [self showPinButton:NO];
    }
}

- (IBAction)tapGestureAction:(UITapGestureRecognizer *)sender
{
    CLLocationCoordinate2D coordinate = [self.mapView convertPoint:[sender locationInView:self.mapView] toCoordinateFromView:self.mapView];
    self.currentPin = [self addNewPinForCoordinate:coordinate];
    [self.mapView addAnnotation:self.currentPin];
    self.tapGestureRecognizer.enabled = NO;
    self.addPinButton.selected = YES;
    [self showPinButton:YES];
}

#pragma mark - UISearchControllerDelegate

- (void)willPresentSearchController:(UISearchController *)searchController
{
    self.addPinButton.selected = NO;
    self.tapGestureRecognizer.enabled = NO;
    self.mapView.userInteractionEnabled = NO;
    [self showPinButton:YES];
}

- (void)willDismissSearchController:(UISearchController *)searchController
{
    self.mapView.userInteractionEnabled = YES;
}

@end
