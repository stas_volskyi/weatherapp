//
//  BaseSearchViewController.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "SearchResultsTableViewController.h"

@interface BaseSearchViewController : UIViewController <UISearchControllerDelegate, UISearchResultsUpdating, UITableViewDelegate, UISearchBarDelegate>

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) SearchResultsTableViewController *resultController;
@property (strong, nonatomic) NSArray<City *> *cities;

- (void)setupSearchBar;

@end
