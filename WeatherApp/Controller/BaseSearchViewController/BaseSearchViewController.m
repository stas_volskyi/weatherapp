//
//  BaseSearchViewController.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "BaseSearchViewController.h"

@interface BaseSearchViewController ()

@end

@implementation BaseSearchViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.cities = [AppHelper knownCities];
    [self setupSearchBar];
}

#pragma mark - Public

- (void)setupSearchBar
{
    SearchResultsTableViewController *resultsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchResultsTableViewController"];
    resultsVC.dataSource = [AppHelper knownCities];
    resultsVC.view.backgroundColor = [UIColor clearColor];
    resultsVC.tableView.delegate = self;
    
    self.resultController = resultsVC;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:resultsVC];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    self.searchController.searchBar.barTintColor = self.view.backgroundColor;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.tintColor = [UIColor whiteColor];
    self.searchController.searchBar.translucent = NO;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name BEGINSWITH[C] %@", searchController.searchBar.text];
    self.resultController.dataSource = [self.cities filteredArrayUsingPredicate:predicate];
    [self.resultController.tableView reloadData];
}

@end
