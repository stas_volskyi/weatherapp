//
//  SearchResultsTableViewController.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class City;

@interface SearchResultsTableViewController : UITableViewController

@property (strong, nonatomic) NSArray<City *> *dataSource;

@end
