//
//  DayForecastCollectionViewCell.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class Forecast;

@interface DayForecastCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
- (void)displayForecastInfo:(Forecast *)forecast;

@end
