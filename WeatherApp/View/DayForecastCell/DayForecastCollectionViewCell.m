//
//  DayForecastCollectionViewCell.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "DayForecastCollectionViewCell.h"
#import "Forecast.h"

@interface DayForecastCollectionViewCell()

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxTempLabel;
@property (weak, nonatomic) IBOutlet UILabel *minTempLabel;

@end

@implementation DayForecastCollectionViewCell

- (void)displayForecastInfo:(Forecast *)forecast
{
    self.dayLabel.text = forecast.dayString;
    self.minTempLabel.text = [NSString stringWithFormat:@"%.1f°", [AppHelper formatTempWithMetrics:forecast.minTemp]];
    self.maxTempLabel.text = [NSString stringWithFormat:@"%.1f°", [AppHelper formatTempWithMetrics:forecast.maxTemp]];
}

@end
