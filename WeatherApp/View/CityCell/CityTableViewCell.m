//
//  CityTableViewCell.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "CityTableViewCell.h"
#import "Weather.h"
#import "City.h"
#import "Forecast.h"

@interface CityTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescription;

@end

@implementation CityTableViewCell

- (void)prepareForReuse
{
    self.weatherImageView.image = nil;
    self.cityNameLabel.text = nil;
    self.tempLabel.text = nil;
    self.weatherDescription.text = nil;
}

- (void)displayWeatherInfo:(Weather *)weather
{
    if (weather.currentForecast) {
        self.cityNameLabel.text = weather.relatedCity.name;
        self.tempLabel.text = [NSString stringWithFormat:@"%.1f°", [AppHelper formatTempWithMetrics:weather.currentForecast.temp]];
        self.weatherDescription.text = weather.currentForecast.weatherDescription;
    } else {
        self.cityNameLabel.text = weather.relatedCity.name;
        self.tempLabel.text = nil;
        self.weatherDescription.text = nil;
        self.weatherImageView.image = nil;
    }
  }

@end
