//
//  CityTableViewCell.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class Weather;

@interface CityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *weatherImageView;
@property (weak, nonatomic) NSURLSessionTask *downloadImageTask;

- (void)displayWeatherInfo:(Weather *)weather;

@end
