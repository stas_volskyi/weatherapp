//
//  WeatherClient.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "WeatherClient.h"
#import "NetworkClient.h"

#import "Forecast.h"

static NSString *const BaseUrl = @"http://api.openweathermap.org/";

@interface WeatherClient()

@property (strong, nonatomic) NetworkClient *networkClient;

@end

@implementation WeatherClient

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self) {
        _networkClient = [[NetworkClient alloc] initWithBaseUrl:BaseUrl];
    }
    return self;
}

#pragma mark - Public

- (NSURLSessionTask *)loadWeatherForCityWithId:(NSInteger)cityId completion:(void (^)(Forecast *forecast, NSError *error))completion
{
    NSString *path = @"data/2.5/weather";
    NSDictionary *params = @{@"id" : @(cityId)};
    return [self.networkClient performGetRequestWithPath:path parameters:params completionHandler:^(id json, NSError *error) {
        if (error && error.code != NSURLErrorCancelled) {
            if (completion) {
                completion(nil, error);
            }
        } else {
            Forecast *forecast = [[Forecast alloc] initWithDictionary:json];
            if (completion) {
                completion(forecast, nil);
            }
        }
    }];
}

- (NSURLSessionTask *)loadWeatherForLatitude:(CGFloat)latitude longitude:(CGFloat)longitude completion:(void (^)(Forecast *forecast, NSError *error))completion
{
    NSString *path = @"data/2.5/weather";
    NSDictionary *params = @{@"lat" : @(latitude),
                             @"lon" : @(longitude)};
    return [self.networkClient performGetRequestWithPath:path parameters:params completionHandler:^(id json, NSError *error) {
        if (error && error.code != NSURLErrorCancelled) {
            if (completion) {
                completion(nil, error);
            }
        } else {
            Forecast *forecast = [[Forecast alloc] initWithDictionary:json];
            if (completion) {
                completion(forecast, nil);
            }
        }
    }];
}

- (NSURLSessionTask *)load5DayWeatherForCityWithId:(NSInteger)cityId completion:(void (^)(NSArray<Forecast *> *forecastArray, NSError *error))completion
{
    NSString *path = @"data/2.5/forecast";
    NSDictionary *params = @{@"id" : @(cityId)};
    return [self.networkClient performGetRequestWithPath:path parameters:params completionHandler:^(id json, NSError *error) {
        if (error && error.code != NSURLErrorCancelled) {
            if (completion) {
                completion(nil, error);
            }
        } else {
            NSArray<Forecast *> *forecastArray = [Forecast fiveDayForecast:json[@"list"]];
            if (completion) {
                completion(forecastArray, nil);
            }
        }
    }];
}

- (NSURLSessionTask *)downloadWeatherImageWithId:(NSString *)imageId completion:(void (^)(UIImage *image, NSError *error))completion
{
    if (!imageId.length) {
        if (completion) {
            completion(nil, nil);
        }
        return nil;
    }
    
    NSString *path = @"img/w/";
    path = [path stringByAppendingPathComponent:imageId];
    path = [path stringByAppendingPathExtension:@"png"];
    return [self.networkClient downloadImageWithPath:path completionHandler:^(UIImage *image, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error && error.code != NSURLErrorCancelled) {
                if (completion) {
                    completion(nil, error);
                }
            } else {
                if (completion) {
                    completion(image, nil);
                }
            }
        });
    }];
}

@end
