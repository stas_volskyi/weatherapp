//
//  WeatherClient.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class Forecast;

@interface WeatherClient : NSObject

- (NSURLSessionTask *)loadWeatherForCityWithId:(NSInteger)cityId completion:(void (^)(Forecast *forecast, NSError *error))completion;
- (NSURLSessionTask *)loadWeatherForLatitude:(CGFloat)latitude longitude:(CGFloat)longitude completion:(void (^)(Forecast *forecast, NSError *error))completion;
- (NSURLSessionTask *)load5DayWeatherForCityWithId:(NSInteger)cityId completion:(void (^)(NSArray<Forecast *> *forecastArray, NSError *error))completion;

- (NSURLSessionTask *)downloadWeatherImageWithId:(NSString *)imageId completion:(void (^)(UIImage *image, NSError *error))completion;

@end
