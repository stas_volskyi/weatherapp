//
//  Forecast.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "Forecast.h"
#import "City.h"

@interface Forecast()

@property (assign, nonatomic, readwrite) CGFloat temp;
@property (assign, nonatomic, readwrite) CGFloat maxTemp;
@property (assign, nonatomic, readwrite) CGFloat minTemp;
@property (assign, nonatomic, readwrite) NSInteger humidity;
@property (assign, nonatomic, readwrite) CGFloat pressure;
@property (assign, nonatomic, readwrite) CGFloat windSpeed;

@end

@implementation Forecast

- (instancetype)initWithDictionary:(NSDictionary *)aDictionary
{
    self = [super init];
    if (self) {
        NSDictionary *main = aDictionary[@"main"];
        _humidity = [AppHelper getInteger:main[@"humidity"]];
        _pressure = [AppHelper getInteger:main[@"pressure"]];
        _temp = [AppHelper getFloat:main[@"temp"]];
        _maxTemp = [AppHelper getFloat:main[@"temp_max"]];
        _minTemp = [AppHelper getFloat:main[@"temp_min"]];
        
        _windSpeed = [AppHelper getFloat:aDictionary[@"wind"][@"speed"]];
        _clouds = [AppHelper getFloat:aDictionary[@"clouds"][@"all"]];
        
        NSDictionary *weather = [aDictionary[@"weather"] firstObject];
        _weatherDescription = [AppHelper getString:weather[@"description"]];
        _iconName = [AppHelper getString:weather[@"icon"]];
        _name = [AppHelper getString:aDictionary[@"name"]];
        
        _uniqueId = [AppHelper getInteger:aDictionary[@"id"]];
        _longitude = [AppHelper getFloat:aDictionary[@"coord"][@"lon"]];
        _latitude = [AppHelper getFloat:aDictionary[@"coord"][@"lat"]];
        
        _timestamp = [AppHelper getDouble:aDictionary[@"dt"]];
    }
    return self;
}

- (NSString *)dayString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, d"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timestamp];
    return [dateFormatter stringFromDate:date];
}

+ (NSArray<Forecast *> *)fiveDayForecast:(NSArray *)list
{
    if (![list isKindOfClass:[NSArray class]] || list.count < 40) {
        NSLog(@"Unexpected 5 day forecast response");
        return nil;
    }
    NSMutableArray *array = [NSMutableArray new];
    
    for (int i = 0; i < 5; i++) {
        NSArray *hoursForecastList = [list subarrayWithRange:NSMakeRange(i * 8, 8)];
        Forecast *forecast = [[Forecast alloc] initWithDictionary:hoursForecastList[6]];
        
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
        NSArray *minTemp = [[hoursForecastList valueForKeyPath:@"main.temp_min"] sortedArrayUsingDescriptors:@[sortDescriptor]];
        forecast.minTemp = [minTemp.firstObject floatValue];
        
        NSArray *maxTemp = [[hoursForecastList valueForKeyPath:@"main.temp_max"] sortedArrayUsingDescriptors:@[sortDescriptor]];
        forecast.maxTemp = [maxTemp.lastObject floatValue];
        
        [array addObject:forecast];
    }
    return array;
}

@end
