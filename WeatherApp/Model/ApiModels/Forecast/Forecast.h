//
//  Forecast.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@interface Forecast : NSObject

@property (assign, nonatomic, readonly) CGFloat temp;
@property (assign, nonatomic, readonly) CGFloat maxTemp;
@property (assign, nonatomic, readonly) CGFloat minTemp;
@property (assign, nonatomic, readonly) NSInteger humidity;
@property (assign, nonatomic, readonly) CGFloat pressure;
@property (assign, nonatomic, readonly) CGFloat windSpeed;
@property (assign, nonatomic, readonly) CGFloat clouds;
@property (assign, nonatomic, readonly) NSTimeInterval timestamp;

@property (strong, nonatomic, readonly) NSString *weatherDescription;
@property (strong, nonatomic, readonly) NSString *iconName;
@property (strong, nonatomic, readonly) NSString *name;

@property (assign, nonatomic, readonly) NSInteger uniqueId;
@property (assign, nonatomic, readonly) CGFloat longitude;
@property (assign, nonatomic, readonly) CGFloat latitude;

- (instancetype)initWithDictionary:(NSDictionary *)aDictionary;
- (NSString *)dayString;

+ (NSArray<Forecast *> *)fiveDayForecast:(NSArray *)list;

@end
