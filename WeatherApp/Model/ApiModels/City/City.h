//
//  City.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class CLLocation;
@class Forecast;

@interface City : NSObject <NSCoding>

@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSString *country;
@property (assign, nonatomic, readonly) NSInteger uniqueId;
@property (strong, nonatomic, readonly) CLLocation *location;

- (instancetype)initWithDictionary:(NSDictionary *)aDictionary;
- (instancetype)initWithForecast:(Forecast *)forecast;

@end
