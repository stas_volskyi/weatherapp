//
//  City.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "City.h"
#import <CoreLocation/CoreLocation.h>
#import "Forecast.h"

@implementation City

- (instancetype)initWithDictionary:(NSDictionary *)aDictionary
{
    self = [super init];
    if (self) {
        NSDictionary *city = aDictionary[@"city"];
        _name = [AppHelper getString:city[@"name"]];
        _country = [AppHelper getString:city[@"country"]];
        _uniqueId = [AppHelper getInteger:city[@"id"]];
        if (!_uniqueId) {
            _uniqueId = [AppHelper getInteger:city[@"id"][@"$numberLong"]];
        }
        CGFloat latitude = [AppHelper getFloat:city[@"coord"][@"lat"]];
        CGFloat longitude = [AppHelper getFloat:city[@"coord"][@"lon"]];
        _location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

    }
    return self;
}

- (instancetype)initWithForecast:(Forecast *)forecast
{
    self = [super init];
    if (self) {
        _name = forecast.name;
        _uniqueId = forecast.uniqueId;
        CGFloat latitude = forecast.latitude;
        CGFloat longitude = forecast.longitude;
        _location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        
    }
    return self;
}

#pragma mark - NSCoder

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        _name = [aDecoder decodeObjectForKey:@"name"];
        _uniqueId = [aDecoder decodeInt64ForKey:@"uniqueId"];
        _country =  [aDecoder decodeObjectForKey:@"country"];
        _location = [aDecoder decodeObjectForKey:@"location"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInt64:self.uniqueId forKey:@"uniqueId"];
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeObject:self.location forKey:@"location"];
}

@end
