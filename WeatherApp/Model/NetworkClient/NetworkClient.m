//
//  NetworkClient.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "NetworkClient.h"

#define ApiKey @"6d52ebde73176539cc7bebd7b87c77e5"

@interface NetworkClient()

@property (strong, nonatomic) NSString *baseUrl;

@end

@implementation NetworkClient

#pragma mark - Init

- (instancetype)initWithBaseUrl:(NSString *)baseUrl
{
    self = [super init];
    if (self) {
        NSParameterAssert(baseUrl);
        _baseUrl = baseUrl;
    }
    return self;
}

#pragma mark - Api Methods

- (NSURLSessionTask *)performGetRequestWithPath:(NSString *)path parameters:(NSDictionary *)parameters completionHandler:(void (^)(id json, NSError *error))completion
{
    NSString *urlString = [self.baseUrl stringByAppendingPathComponent:path];
    return [self performGetRequestWithUrl:urlString parameters:parameters completionHandler:completion];
}

- (NSURLSessionTask *)performGetRequestWithUrl:(NSString *)urlString parameters:(NSDictionary *)parameters completionHandler:(void (^)(id json, NSError *error))completion
{
    NSParameterAssert(self.baseUrl);
    NSParameterAssert(urlString);
    NSParameterAssert(completion);
    
    urlString = [urlString stringByAppendingString:@"?"];
    if (parameters.allKeys.count) {
        for (NSString *key in parameters.allKeys) {
            BOOL isFirstParam = [urlString hasSuffix:@"?"];
            id value = parameters[key];
            urlString = [urlString stringByAppendingFormat:@"%@%@=%@", (isFirstParam ? @"" : @"&"), key, value];
        }
    }
    urlString = [urlString stringByAppendingFormat:@"&ApiKey=%@", ApiKey];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            completion(nil, error);
        } else {
            if (data) {
                NSError *jsonError;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                if (jsonError) {
                    completion(nil, jsonError);
                } else {
                    completion(json, nil);
                }
            } else {
                completion(nil, nil);
            }
        }
    }];
    [downloadTask resume];
    return downloadTask;
}

- (NSURLSessionTask *)downloadImageWithPath:(NSString *)path completionHandler:(void (^)(UIImage *image, NSError *error))completion
{
    NSParameterAssert(self.baseUrl);
    NSParameterAssert(path);
    NSParameterAssert(completion);
    
    NSString *urlString = [self.baseUrl stringByAppendingPathComponent:path];
    urlString = [urlString stringByAppendingString:@"?"];
    urlString = [urlString stringByAppendingFormat:@"&ApiKey=%@", ApiKey];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            completion(nil, error);
        } else {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                completion(image, nil);
            } else {
                completion(nil, nil);
            }
        }
    }];
    [downloadTask resume];
    return downloadTask;
}

@end
