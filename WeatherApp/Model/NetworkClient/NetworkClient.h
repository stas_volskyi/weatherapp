//
//  NetworkClient.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/18/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@interface NetworkClient : NSObject

- (instancetype)initWithBaseUrl:(NSString *)baseUrl;

- (NSURLSessionTask *)performGetRequestWithPath:(NSString *)path parameters:(NSDictionary *)parameters completionHandler:(void (^)(id json, NSError *error))completion;
- (NSURLSessionTask *)performGetRequestWithUrl:(NSString *)urlString parameters:(NSDictionary *)parameters completionHandler:(void (^)(id json, NSError *error))completion;
- (NSURLSessionTask *)downloadImageWithPath:(NSString *)path completionHandler:(void (^)(UIImage *image, NSError *error))completion;

@end
