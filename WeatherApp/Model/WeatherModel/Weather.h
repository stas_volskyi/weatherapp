//
//  Weather.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class Forecast;
@class City;

@interface Weather : NSObject

@property (strong, nonatomic) Forecast *currentForecast;
@property (strong, nonatomic) City *relatedCity;

@end
