//
//  BookmarkStorage.h
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

@class City;

@interface BookmarkStorage : NSObject

- (BOOL)addBookmark:(City *)city;
- (void)removeBookmark:(City *)city;
- (void)clearBookmarks;

- (NSArray<City *> *)getBookmarks;
- (BOOL)isCityInBookmarks:(City *)city;

- (void)createDefaultBookmarks;

@end
