//
//  BookmarkStorage.m
//  WeatherApp
//
//  Created by Stanislav Volskyi on 6/19/17.
//  Copyright © 2017 StasVoslkyi. All rights reserved.
//

#import "BookmarkStorage.h"
#import "City.h"

@interface BookmarkStorage()

@property (strong, nonatomic) NSFileManager *fileManager;
@property (strong, nonatomic) NSString *filePath;
@property (strong, nonatomic) NSArray<City *> *bookmarks;

@end

@implementation BookmarkStorage

#pragma mark - Init

- (instancetype)init
{
    if (self) {
        _fileManager = [NSFileManager defaultManager];
        _filePath = [[self.fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].firstObject.path stringByAppendingPathComponent:@"Bookmarks"];
        [self loadBookmarks];
    }
    return self;
}

#pragma mark - Private

- (void)loadBookmarks
{
    if ([self.fileManager fileExistsAtPath:self.filePath]) {
        NSData *data = [NSData dataWithContentsOfFile:self.filePath];
        self.bookmarks = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    if (!self.bookmarks) {
        self.bookmarks = [NSArray array];
    }
}

#pragma mark - Bookmark managment

- (BOOL)addBookmark:(City *)city
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uniqueId == %li", city.uniqueId];
    if (![self.bookmarks filteredArrayUsingPredicate:predicate].count) {
        self.bookmarks = [self.bookmarks arrayByAddingObject:city];
        if (![NSKeyedArchiver archiveRootObject:self.bookmarks toFile:self.filePath]) {
            NSLog(@"Failed to save bookmark");
        }
        return YES;
    }
    return NO;
}

- (void)removeBookmark:(City *)city
{
    NSMutableArray *mutableBookmarks = self.bookmarks.mutableCopy;
    [mutableBookmarks removeObject:city];
    if (![NSKeyedArchiver archiveRootObject:mutableBookmarks toFile:self.filePath]) {
        NSLog(@"Failed to delete bookmark");
    }
    self.bookmarks = mutableBookmarks;

}

- (void)clearBookmarks
{
    NSError *error;
    [self.fileManager removeItemAtPath:self.filePath error:&error];
    if (error) {
        NSLog(@"Failed to clean bookmarks. %@", error);
    }
    self.bookmarks = [NSArray array];
}

- (NSArray<City *> *)getBookmarks
{
    return _bookmarks;
}

- (BOOL)isCityInBookmarks:(City *)city
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uniqueId == %li", city.uniqueId];
    return [self.bookmarks filteredArrayUsingPredicate:predicate].count;
}

- (void)createDefaultBookmarks
{
    if (![self.fileManager fileExistsAtPath:self.filePath]) {
        NSArray *cities = [AppHelper knownCities];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name LIKE[c] %@ && country LIKE[c] %@", @"London", @"GB"];
        City *city = [cities filteredArrayUsingPredicate:predicate].firstObject;
        [self addBookmark:city];
        
        predicate = [NSPredicate predicateWithFormat:@"name LIKE[c] %@ && country LIKE[c] %@", @"Amsterdam", @"NL"];
        city = [cities filteredArrayUsingPredicate:predicate].firstObject;
        [self addBookmark:city];
        
        predicate = [NSPredicate predicateWithFormat:@"name LIKE[c] %@ && country LIKE[c] %@", @"Paris", @"FR"];
        city = [cities filteredArrayUsingPredicate:predicate].firstObject;
        [self addBookmark:city];
    }
}

@end
